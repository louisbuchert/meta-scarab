clean minimal layer to boot the Lichee Pi

Works under Dunfell


local.conf:

MACHINE = "scarab"
WPA-SUPPLICANT_SSID = "your_id"
WPA-SUPPLICANT_PSK = "your_psk_generated_with_wpa_passphrase"
ROOT_PASSWORD = "your_root_password_generated_with_c_crypt_function"


bblayers.conf:

BBLAYERS ?= " \
  /home/louisbuchert/Projects/yoctoproject/layers/poky/meta \
  /home/louisbuchert/Projects/yoctoproject/layers/poky/meta-poky \
  /home/louisbuchert/Projects/yoctoproject/layers/poky/meta-yocto-bsp \
  /home/louisbuchert/Projects/yoctoproject/layers/meta-openembedded/meta-oe \
  /home/louisbuchert/Projects/yoctoproject/layers/meta-sunxi \
  /home/louisbuchert/Projects/yoctoproject/layers/meta-scarab \
  "


Image name:
scarab-image
