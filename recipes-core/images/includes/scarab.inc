SUMMARY = "Scarab image include"

LICENSE = "MIT"

inherit core-image
inherit extrausers

WIFI_SUPPORT = " \
                init-ifupdown \
                wpa-supplicant \
                wpa-supplicant-passphrase \
                network-config-misc \
                esp8089-mod \
               "

GSTREAMER_SUPPORT = " \
                     gstreamer1.0 \
                     gstreamer1.0-plugins-base \
                     gstreamer1.0-plugins-good \
                    "

IMAGE_INSTALL += " \
  ${WIFI_SUPPORT} \
  ${GSTREAMER_SUPPORT} \
"

DISTRO_FEATURES += "systemd"
VIRTUAL-RUNTIME_init_manager = "systemd"

EXTRA_USERS_PARAMS += "usermod -p '${ROOT_PASSWORD}' root;"
